#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
import time
import readline
import poloniex
import bittrex
# import modulos.api_keys as keys
import modulos.pyTemplate as ptemp
from modulos.arby import ArbyScanner, get_poloniex_prices, get_bittrex_prices
from modulos.stoplimit import stopLimit

# Arby config
global diff
global timeframe
diff = 0.05
timeframe = 60

# inicia temporizador
start_time = time.time()


# Funciones

# imprime el balance de Poloniex,
def imprime_balance(cuenta):
    print cuenta


# devuelve el balance de una coin
def check_balance(coin):
    return polo.returnBalances()[coin]


# crea un raw_input con un valor ya escrito
def rlinput(prompt, prefill=''):
    readline.set_startup_hook(lambda: readline.insert_text(prefill))
    try:
        return raw_input(prompt)
    finally:
        readline.set_startup_hook()


def rellena_stoplimit():
    try:
        sl = stopLimit()

        # seleccion  del modo
        app.limpia_pantalla()
        sl.setModo()

        # selecion del market
        app.limpia_pantalla()
        sl.setMarket(polo.returnTicker())

        # seleccion de la cantidad
        if len(sl.modo) == 3:
            balances = polo.returnAvailableAccountBalances()
        else:
            balances = polo.returnTradableBalances()
        app.limpia_pantalla()
        sl.setAmount(balances)

        # seleccion de stop y limit
        app.limpia_pantalla()
        sl.setStop(polo.returnTicker()[sl.market])
        sl.setLimit()
        return sl
    except KeyboardInterrupt:
        app.salir()


# Inicio del script
if __name__ == "__main__":
    # creamos el objeto de la aplicación y exchanges
    app = ptemp.Aplicacion(
        "pyPoloniex",
        "Utilidades para poloniex",
        "https://gitlab.com/srmojon/pyPoloniex"
    )
    #polo = poloniex.Poloniex(keys.poloniex_key, keys.poloniex_secret)
    #btx = bittrex.Bittrex(keys.bittrex_key, keys.bittrex_secret)

    # definimos las opciones
    app.add_op_boolean("--balances", "Imprime todos los balances en pantalla.")

    app.add_op_boolean("--arby", "Adaptación del scanner arby.py")
    app.add_op_valor("-p", "Porcentaje para arby (Default: 0.05 = 5%%)", float)
    app.add_op_valor("-t", "Tiempo en segundos para arby (Default: 60)", int)

    # TODO: falta el modo buy-margin y sell-margin
    app.add_op_boolean("--stoplimit", "Crear orden Stop-Limit")
    args = app.check_args()

    # comprobamos todos los parametros
    if args.balances:
        # print polo.returnAvailableAccountBalances()
        check_balance("STR")
        # check_balance("margin")
        # check_balance("lending")

    # Adaptación del script arby.py:
    if args.arby:
        # asignamos los parametros del usuario si los hay
        if args.p is not None:
            diff = args.p
        if args.t is not None:
            timeframe = args.t

        # creamos objeto para arby scanner
        arby = ArbyScanner(diff)

        # cargamos listas de los exchanges que usemos
        pricePolo = get_poloniex_prices(polo)
        priceBtx = get_bittrex_prices(btx)

        # comparamos listas
        try:
            while True:
                app.limpia_pantalla()
                arby.scan(pricePolo, priceBtx, "Poloniex", "Bittrex")
                ptemp.cuenta_atras("  Escaneando de nuevo en ", timeframe)
        except KeyboardInterrupt:
            app.salir()
        except:
            print sys.exc_info()

    # funcion stop-limit
    if args.stoplimit:
        # creamos objeto y solicitamos los datos
        sl = rellena_stoplimit()

        # mostramos resumen y pedimos confirmación
        try:
            app.limpia_pantalla()
            sl.resumen()
        except KeyboardInterrupt:
            app.salir()

        try:
            order = False
            while True:
                # si cumple con la config...
                if sl.check(polo.returnTicker()[sl.market]):
                    # ejecutamos orden
                    if sl.modo == "buy":
                        order = polo.buy(sl.market, sl.limit, sl.amount)
                    elif sl.modo == "sell":
                        order = polo.sell(sl.market, sl.limit, sl.amount)
                    elif sl.modo == "buy-margin":
                        order = polo.marginBuy(sl.market, sl.limit, sl.amount)
                    elif sl.modo == "sell-margin":
                        order = polo.marginSell(sl.market, sl.limit, sl.amount)
                # si la orden se ha ejecutado...
                if order is not False:
                    print "  Orden abierta: " + str(order['orderNumber'])
                    break
                ptemp.cuenta_atras(30)
        except KeyboardInterrupt:
            sys.exit()

    # Calcula y muestra el tiempo de ejecucion
    elapsed_time = time.time() - start_time
    print "\n * Duración del script: %.8f segundos." % elapsed_time
