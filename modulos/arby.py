#!/usr/bin/python
# -*- coding: utf-8 -*-

import string
import urllib
import json


class ArbyScanner():
    def __init__(self, diff):
        self.percent = str(diff * 100) + "%"
        self.diff = diff

    # compara listas de precios
    def scan(self, t1, t2, name1, name2):
        print "  Comparando precios entre %s y %s:\n" % (name1, name2)
        # cada mercado de el ticker1 se compara con el del ticker2
        for item1 in t1:
            market1, precio1_str = item1.split(":")
            price1 = float(precio1_str)
            for item2 in t2:
                market2, precio2_str = item2.split(":")
                price2 = float(precio2_str)
                # si el mercado existe en los dos...
                if market1 == market2:
                    diferencia = abs(price1 - price2) / price2
                    # y la diferencia es mayor al porcentaje minimo...
                    if diferencia >= self.diff:
                        self.alerta(market1, name1, price1, name1, price2)

    # le damos 2 exchanges con sus precios y nombres, nos devuelve el string
    def alerta(self, market, m1_name, m1_price, m2_name, m2_price):
        xcent = abs(abs(m1_price - m2_price) / m2_price) * 100
        percent = str("%.2f" % xcent) + "%"
        m1_str = str("%.8f" % m1_price)
        m2_str = str("%.8f" % m2_price)
        if m1_price < m2_price:
            r1 = "\tCompra %s en: %s a %s\n" % (market, m1_name, m1_str)
            r2 = "\tVende en %s a %s\n" % (m2_name, m2_str)
            r3 = "\tProfit: %s\n" % percent
        else:
            r1 = "\tCompra %s en: %s a %s\n" % (market, m2_name, m2_str)
            r2 = "\tVende en %s a %s\n" % (m1_name, m1_str)
            r3 = "\tProfit: %s\n" % percent
        print r1 + r2 + r3


# convierte un float en un string con 8 decimales
def float2str(valor):
    valor = float(valor)
    return "%.8f" % valor


# quita simbolos de los mercados
def normalize_market_name(market):
    market = string.replace(market, '-', '')
    market = string.replace(market, '_', '')
    return market


# Funciones

# Exchanges - cada funcion devuelve una lista para usarla con compara_precios()
def get_poloniex_prices(polo):
    polotick = polo.returnTicker()
    lista = []
    for market, v1 in polotick.iteritems():
        market = normalize_market_name(market)
        last = float2str(float(v1['last']))
        entrada = str(market) + ":" + last
        lista.append(entrada)
    return lista


# OK - devuelve lista "market:precio" de bittrex
def get_bittrex_prices(btx):
    btrxtick = btx.get_market_summaries()
    lista = []
    for bit in btrxtick['result']:
        market = normalize_market_name(bit['MarketName'])
        last = float2str(float(bit['Last']))
        entrada = "%s:%s" % (str(market), last)
        lista.append(entrada)
    return lista


# TODO: Las funciones siguientes fallan, se tienen que adaptar...
# devuelve lista "market:precio" de binance
def get_binance_prices():
    url = "https://www.binance.com/api/v1/ticker/allPrices"
    response = urllib.urlopen(url)
    bintick = json.loads(response.read())
    lista = []
    for bince in bintick:
        market = normalize_market_name(bince['symbol'])
        last = float(bince['price'])
        entrada = "%s:%s" % (str(market), str(last))
        lista.append(entrada)
    return lista


# devuelve lista "market:precio" de exmo
def get_exmo_prices():
    url = "https://api.exmo.com/v1/ticker/"
    response = urllib.urlopen(url)
    pairs = json.loads(response.read())
    lista = []
    for pair in pairs:
        last = float(pairs[pair]['last_trade'])
        c1, c2 = pair.split("_")
        market = c2 + c1
        market2 = normalize_market_name(pair)
        entrada = "%s:%.8f" % (str(market), last)
        entrada2 = "%s:%.8f" % (str(market2), last)
        lista.append(entrada)
        lista.append(entrada2)
    return lista


# devuelve lista "market:precio" de bitfinex
def get_bitfinex_prices():
    # finex = bitfinex.Client()
    # markets = finex.symbols()
    lista = []
    # for market in markets:
    #     ticker = finex.ticker(market)
    #     last = float(ticker['last_price'])
    #     entrada = "%s:%.8f" % (str(market), last)
    #     lista.append(entrada)
    return lista


# devuelve lista "market:precio" de kraken
def get_kraken_prices():
    url = "https://api.kraken.com/0/public/AssetPairs"
    response = urllib.urlopen(url)
    assets = json.loads(response.read())
    pairs = assets['result']
    print pairs
    for pair in pairs:
        print pairs[pair]['altname']
    lista = []
    return lista


if __name__ == "__main__":
    print "\n\tEste modulo no puede ser ejecutado como script."
    print "\tVer documentación y ejemplos en README.md\n"
else:
    pass
