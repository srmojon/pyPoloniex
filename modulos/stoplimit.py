#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import pyTemplate as P


class stopLimit:
    def __init__(self):
        self.modos = ["buy", "sell", "buy-margin", "sell-margin"]
        self.margin_markets = [
            "BTC_STR", "BTC_ETH", "BTC_XRP", "BTC_LTC",
            "BTC_XMR", "BTC_DASH", "BTC_BTS", "BTC_CLAM",
            "BTC_DOGE", "BTC_MAID", "BTC_FCT"
            ]

    # Muestra lista de modos
    def setModo(self):
        n = P.lista_menu(self.modos, "Que tipo de orden quieres hacer?")
        self.modo = self.modos[n]

    # seleccion del market
    def setMarket(self, tick):
        if len(self.modo) == 3:
            while True:
                coin = P.rlinput("  Con que moneda quieres operar? ")
                markets = []
                for pair in tick:
                    if pair.find(coin.upper()) != -1:
                        markets.append(pair)
                if len(markets) > 0:
                    break
                P.err("No hay markets con esa altcoin.")
            n = P.lista_menu(markets, "Selecciona un market:")
            self.market = markets[n]
        else:
            n = P.lista_menu(self.margin_markets, "Selecciona un market:")
            self.market = self.margin_markets[n]

    # seleccion de cantidad
    def setAmount(self, balances):
        coin1, coin2 = self.market.split("_")
        # compra en cuenta 'exchange'
        if self.modo == "buy":
            balance = float2str(balances[coin1])
            default = balance
            coin = coin1
            texto = "\n  Cantidad que quieres usar: "
            print "  Comprar %s en cuenta 'exchange'" % coin2

        # venta en cuenta 'exchange'
        elif self.modo == "sell":
            balance = float2str(balances[coin2])
            default = balance
            coin = coin2
            texto = "\n  Cantidad que quieres vender: "
            print "  Vender %s en cuenta 'exchange'" % coin2

        # compra en cuenta 'margin'
        elif self.modo == "buy-margin":
            balance = float2str(balances[self.market][coin1])
            default = float(balance) / 5
            coin = coin1
            texto = "\n  Cantidad que quieres usar: "
            print "  Comprar %s en cuenta 'margin'" % coin2

        # venta en cuenta 'margin'
        elif self.modo == "sell-margin":
            balance = float2str(balances[self.market][coin2])
            default = float(balance) / 5
            coin = coin2
            texto = "\n  Cantidad que quieres vender: "
            print "  Vender %s en cuenta 'margin'" % coin2

        # pide la cantidad al usuario
        print "    Balance disponible: %s %s" % (balance, coin)
        while True:
            amount = P.rlinput(texto, default)
            if float(amount) <= float(balance):
                break
            P.err("Cantidad incorrecta.")
        self.amount = float(amount)
        self.coin = coin2

    # pide valor de STOP
    def setStop(self, tick):
        while True:
            high = float(tick['highestBid'])
            low = float(tick['lowestAsk'])
            last = float(tick['last'])
            print "  Last: " + float2str(last)
            print "  Highest Bid: " + float2str(high)
            print "  Lowest Ask: " + float2str(low)
            texto = "\n  Introduce el valor de STOP: "
            default = float2str(last)
            try:
                self.stop = float(P.rlinput(texto, default))
                break
            except:
                P.err("Valor no valido.")
            os.system('clear')

    # pide valor de LIMIT
    def setLimit(self):
        texto = "\n  Introduce el valor de LIMIT: "
        default = float2str(self.stop)
        while True:
            try:
                self.limit = float(P.rlinput(texto, default))
                break
            except:
                P.err("Valor no valido.")

    # muestra resumen
    def resumen(self):
        amount2 = float(self.amount)
        if self.modo == "buy":
            amount2 = float(self.amount) / float(self.limit)
        print "  Configuración del Stop-Limit:"
        print "    Tipo: " + self.modo
        print "    Mercado: " + self.market
        print "    Cantidad: %s %s" % (float2str(amount2), self.coin)
        print "    Stop:  " + float2str(self.stop)
        print "    Limit: " + float2str(self.limit)

    # intenta colocar la orden
    def check(self, tick):
        high = float(tick['highestBid'])
        low = float(tick['lowestAsk'])
        if self.modo[:3] == "sell":
            if self.stop <= high:
                respuesta = True
            else:
                dif = self.stop - high
                print "  [%.8f] Falta %.8f para llegar al stop" % (high, dif)
        elif self.modo[:3] == "buy":
            if self.stop >= low:
                respuesta = True
            else:
                dif = low - self.stop
                print "  [%.8f] Falta %.8f para llegar al stop" % (low, dif)
        return respuesta

# funciones
# convierte un float en un string con 8 decimales
def float2str(valor):
    valor = float(valor)
    return "%.8f" % valor
